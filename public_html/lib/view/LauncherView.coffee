
define [ 'underscore' ], (_) ->
  VIEW_WIDTH = 800
  VIEW_HEIGHT = 600

  MAX_DRAG_LENGTH = 100
  FLYER_TAP_RADIUS = 10

  class LauncherView
    constructor: (launcher, parentStage) ->
      @stage = parentStage.createViewportStage VIEW_WIDTH, VIEW_HEIGHT

      @stage.translateView launcher.flyer.x - VIEW_WIDTH * 0.5, launcher.flyer.y - VIEW_HEIGHT * 0.5

      @platformSpriteList = (
        @stage.createSprite(p.w, 3, 0, 0).translate(p.x, p.y).setClass('platform')
      ) for p in launcher.platformList

      @flyerSprite = @stage.createSprite(10, 10, 5, 5).translate(launcher.flyer.x, launcher.flyer.y).setClass('flyer')

      @stage.on 'tap', (tap) =>
        tapDX = launcher.flyer.x - tap.x
        tapDY = launcher.flyer.y - tap.y

        if launcher.flyer.isLanded && tapDX * tapDX + tapDY * tapDY < FLYER_TAP_RADIUS * FLYER_TAP_RADIUS
          slingshotSprite = @stage.createSprite(1, 4, 0, 2).translate(tap.startX, tap.startY).setClass('slingshot')

          dx = 0
          dy = 0

          tap.on 'moved', =>
            dx = tap.x - tap.startX
            dy = tap.y - tap.startY

            length = Math.sqrt(dx * dx + dy * dy)

            if length > MAX_DRAG_LENGTH
                dx *= MAX_DRAG_LENGTH / length
                dy *= MAX_DRAG_LENGTH / length
                length = MAX_DRAG_LENGTH

            slingshotSprite.translateRotateScale launcher.flyer.x, launcher.flyer.y, Math.atan2(-dy, -dx), length, 1

          tap.once 'released', =>
            slingshotSprite.remove()

            launcher.launchFlyer -dx * 6, -dy * 6

      launcher.on 'flyer.moved', =>
        @flyerSprite.translate launcher.flyer.x, launcher.flyer.y
        @stage.translateView launcher.flyer.x - VIEW_WIDTH * 0.5, launcher.flyer.y - VIEW_HEIGHT * 0.5

      launcher.on 'flyer.landed', =>
        @flyerSprite.translateRotate launcher.flyer.x, launcher.flyer.y, 2
        @stage.translateView launcher.flyer.x - VIEW_WIDTH * 0.5, launcher.flyer.y - VIEW_HEIGHT * 0.5
