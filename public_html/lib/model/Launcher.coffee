
define [ 'underscore', 'EventEmitter' ], (_, EventEmitter) ->
  FIELD_WIDTH = 700
  FIELD_HEIGHT = 600

  TIME_STEP = 0.02

  animate = (step, handler) ->
    id = setInterval (->
      handler step
    ), step * 1000

    # return stopping hook
    stop: () ->
      clearInterval id

  class Launcher extends EventEmitter
    constructor: ->
      @platformList = [
        { x: 0, y: FIELD_HEIGHT, w: FIELD_WIDTH }
      ].concat({
        x: _.random FIELD_WIDTH - 200
        y: _.random 100, 500
        w: _.random 50, 200
      } for i in [0...10])

      @flyer = {
        x: 75, y: 50,
        vx: 20, vy: -10,
        isLanded: true
      }

    collide: (x1, y1, x2, y2, report, otherwise) ->
      collided = false

      # only collide when falling
      if y2 > y1
        for p in @platformList
          # blah blah linear algebra vector intersection
          px1 = p.x
          px2 = px1 + p.w
          py = p.y

          coeff = (py - y1) / (y2 - y1)

          if coeff >= 0 and coeff <= 1
            where = x1 + (x2 - x1) * coeff
            if where >= px1 and where <= px2
              # report only the first found collision
              collided = true
              report p, where
              break

      if not collided
        otherwise()

    launchFlyer: (vx, vy) ->
      throw 'must be landed' if not @flyer.isLanded

      @flyer.isLanded = false
      @flyer.vx = vx
      @flyer.vy = vy

      gravity = 20;
      x = @flyer.x
      y = @flyer.y
      dx = vx * TIME_STEP
      dy = vy * TIME_STEP

      flight = animate TIME_STEP, () =>
        oldX = x
        oldY = y

        dy += gravity * TIME_STEP
        x += dx
        y += dy

        # bounds checking
        if x < 0
          x = 0
          dx = -dx

        if x > 700
          x = 700
          dx = -dx

        # check for platform collision
        @collide oldX, oldY, x, y, ((platform, collisionX) =>
          flight.stop()

          # move flyer exactly to the collision spot
          @flyer.x = collisionX
          @flyer.y = platform.y
          @flyer.isLanded = true

          @trigger 'flyer.landed'
        ), (() =>
          @flyer.x = x
          @flyer.y = y

          @trigger 'flyer.moved'
        )



