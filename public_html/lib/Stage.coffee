
define [ 'underscore', 'jquery', 'EventEmitter' ], (_, $, EventEmitter) ->

  class Sprite
    constructor: ($stage, w, h, cx, cy) ->
      @$div = $('<div></div>').appendTo($stage)

      @$div.css({ 'width': "#{ w }px", 'height': "#{ h }px" })

      origin = "#{ (cx ? w * 0.5) }px #{ (cy ? h * 0.5) }px"
      @$div.css { '-moz-transform-origin': origin, '-webkit-transform-origin': origin }

      @centerXform = "translate(#{ -(cx ? w * 0.5) }px,#{ -(cy ? h * 0.5) }px)"

    setClass: (className) ->
      @$div.attr 'class', className
      this

    translate: (x, y) ->
      xform = "#{ @centerXform } translate(#{ x }px,#{ y }px)"
      @$div.css { '-moz-transform': xform, '-webkit-transform': xform }
      this

    translateRotate: (x, y, r) ->
      xform = "#{ @centerXform } translate(#{ x }px,#{ y }px) rotate(#{ r }rad)"
      @$div.css { '-moz-transform': xform, '-webkit-transform': xform }
      this

    translateRotateScale: (x, y, r, sx, sy) ->
      xform = "#{ @centerXform } translate(#{ x }px,#{ y }px) rotate(#{ r }rad) scale(#{ sx }, #{ sy })"
      @$div.css { '-moz-transform': xform, '-webkit-transform': xform }
      this

    remove: ->
      @$div.remove()
      @$div = null
      this

  class ViewportTap extends EventEmitter
    constructor: (viewport, tap) ->
      @startX = tap.startX + viewport.vx
      @startY = tap.startY + viewport.vy

      @x = @startX
      @y = @startY

      anyMoveTracker = () =>
        @x = tap.x + viewport.vx
        @y = tap.y + viewport.vy
        @trigger 'moved'

      tap.on 'moved', anyMoveTracker
      viewport.on 'view.moved', anyMoveTracker

      tap.once 'released', =>
        tap.off 'moved', anyMoveTracker
        viewport.off 'view.moved', anyMoveTracker
        @trigger 'released'

  class ViewportStage extends EventEmitter
    constructor: (stage, $stage, w, h) ->
      @w = w
      @h = h

      @vx = 0
      @vy = 0

      @$clip = $('<div></div>').appendTo($stage)
      @$clip.css({ 'width': "#{ w }px", 'height': "#{ h }px", 'overflow': 'hidden' })

      @$view = $('<div></div>').appendTo(@$clip)

      stage.on 'tap', (tap) =>
        # ignore out-of-bounds
        # @todo bounds check

        @trigger 'tap', [ new ViewportTap this, tap ]

    translate: (x, y) ->
      xform = "translate(#{ x }px,#{ y }px)"
      @$clip.css { '-moz-transform': xform, '-webkit-transform': xform }
      this

    translateView: (x, y) ->
      @vx = x
      @vy = y

      xform = "translate(#{ -x }px,#{ -y }px)"
      @$view.css { '-moz-transform': xform, '-webkit-transform': xform }

      @trigger 'view.moved'
      this

    createSprite: (w, h, cx, cy) ->
      return new Sprite @$view, w, h, cx, cy

  class StageTap extends EventEmitter
    constructor: ($stage, event) ->
      startTouch = if event.originalEvent.changedTouches then event.originalEvent.changedTouches[0] else event
      offset = $stage.offset()

      @touchId = startTouch.identifier
      @startX = startTouch.pageX - offset.left
      @startY = startTouch.pageY - offset.top

      @x = @startX
      @y = @startY

      withCurrentTouch = (event, handler) =>
        if event.originalEvent.changedTouches
          for t in event.originalEvent.changedTouches when t.identifier is @touchId
            handler t
        else
          handler event

      moveTracker = (event) =>
        withCurrentTouch event, (touch) =>
          @x = touch.pageX - offset.left
          @y = touch.pageY - offset.top
          @trigger 'moved'

      endTracker = (event) =>
        withCurrentTouch event, (touch) =>
          $(document).off 'mousemove touchmove', moveTracker
          $(document).off 'mouseup touchend', endTracker
          @trigger 'released'

      # not using "once" listener for "touchend" because might receive multiple ignorable events
      $(document).on 'mousemove touchmove', moveTracker
      $(document).on 'mouseup touchend', endTracker

  class Stage extends EventEmitter
    constructor: () ->
      # @todo randomize ID and inject style dynamically
      @$stage = $('<div id="stage"></div>').appendTo('body')

      @$stage.on 'mousedown touchstart', (event) =>
        tap = new StageTap @$stage, event

        @trigger 'tap', [ tap ]

    createViewportStage: (w, h) ->
      return new ViewportStage this, @$stage, w, h

    createSprite: (w, h, cx, cy) ->
      return new Sprite @$stage, w, h, cx, cy
