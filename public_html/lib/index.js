
require([
    'jquery',
    'cs!Stage',
    'cs!./model/Launcher',
    'cs!./view/LauncherView'
], function ($, Stage, Launcher, LauncherView) {
    var stage = new Stage(),
        launcher = new Launcher(),
        launcherView = new LauncherView(launcher, stage);

    launcher.launchFlyer(20, -10);
});
